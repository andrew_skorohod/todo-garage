from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Project(models.Model):
    author = models.ForeignKey(User,on_delete=models.CASCADE,verbose_name='author')
    title = models.CharField(max_length=255)
    date = models.DateTimeField( auto_now=False, auto_now_add=True )

    def __str__(self):
        return self.title


class Task(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    project = models.ForeignKey(Project,on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    date = models.DateTimeField(auto_now=False, auto_now_add=True)
    status = models.BooleanField(default='False')
    position = models.ForeignKey('self',models.SET_NULL,blank=True,null=True)#related to next in position
    deadline = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.title
