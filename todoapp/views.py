from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse, Http404
from django.shortcuts import render

from .models import Project, Task


@login_required(login_url='/accounts/login/')
def main(request):
    projects = Project.objects.filter(author=request.user.id).order_by('-date')
    context = {
        'projects': projects,
    }
    return render(request, 'main.html', context)


@login_required(login_url='/accounts/login/')
def projectDetail(request, id):
    if request.user.is_authenticated():
        try:
            tasks = Task.objects.filter(project=id)
        except Task.DoesNotExist:
            raise Http404("No tasks with that id")
        else:
            task_sorted = []

            def start():
                for task in tasks:
                    if task.position is None:
                        task_sorted.append(task)
                        recursive(task)

            def recursive(t):
                for task in tasks:
                    if task.position == t:
                        task_sorted.append(task)
                        recursive(task)

            start()
            try:
                project = Project.objects.get(id=id)
            except Project.DoesNotExist:
                raise Http404("No tasks with that id")
            context = {
                'tasks': task_sorted,
                'project': project,
            }
            return render(request, 'projectDetail.html', context)
    else:
        return render(request, 'sign_pls.html')


def addProject(request):
    author_id = request.GET.get('user')
    user = User.objects.get(id=author_id)
    title = request.GET.get('title')
    obj = Project(author=user, title=title)
    obj.save()
    return HttpResponse(obj.id)


def addTask(request):
    user = request.GET.get('user')
    author = User.objects.get(id=user)
    project = Project.objects.get(id=request.GET.get('project'))
    title = request.GET.get('title')
    deadline = request.GET.get('deadline')
    if len(deadline) > 0:
        obj = Task(author=author, project=project, title=title, deadline=deadline)
    else:
        obj = Task(author=author, project=project, title=title, )
    obj.save()
    nextId = request.GET.get('nextId')
    if nextId is not None:
        nextId = nextId[4:]
        obj2 = Task.objects.get(id=nextId)
        obj2.position = obj
        obj2.save()
    return HttpResponse(obj.id)


def change(request):
    id = request.GET.get('id')
    status = request.GET.get('status')
    if status == 'true':
        status = True
    else:
        status = False
    task = Task.objects.get(id=id)
    task.status = status
    task.save()
    return HttpResponse('ok')


def changepos(request):
    id = request.GET.get('id')
    id = id[4:]
    nextId = request.GET.get('nextId')
    print(nextId)
    obj1 = Task.objects.get(id=id)
    try:
        obj2 = Task.objects.get(position=obj1.id)
    except Task.DoesNotExist:
        obj2 = None
    else:
        obj2.position = obj1.position
        obj2.save()

    if nextId is None:
        pId = request.GET.get('pId')
        obj4 = Task.objects.get(id=pId[4:])
        obj1.position = obj4
        obj1.save()

    else:
        nextId = nextId[4:]
        obj3 = Task.objects.get(id=nextId)
        obj1.position = obj3.position
        obj3.position = obj1
        obj1.save()
        obj3.save()
    return HttpResponse('ok')


def deleteProject(request):
    id = request.GET.get('project')
    user = request.GET.get('user')
    author = User.objects.get(id=user)
    obj = Project.objects.get(id=id)
    if obj.author == author:
        obj.delete()
        return HttpResponse('ok')
    else:
        return HttpResponse('failfish')


def deleteTask(request):
    id = request.GET.get('task')
    user = request.GET.get('user')
    author = User.objects.get(id=user)
    obj = Task.objects.get(id=id)

    if obj.author == author:
        try:
            obj2 = Task.objects.get(position=obj)
        except Task.DoesNotExist:
            obj2 = None
        else:
            obj2.position = obj.position
            obj2.save()
            obj.delete()

        return HttpResponse('ok')
    else:
        return HttpResponse('failfish')


def editProject(request):
    id = request.GET.get('id')
    user = request.GET.get('user')
    text = request.GET.get('text')
    author = User.objects.get(id=user)
    obj = Project.objects.get(id=id)
    if obj.author == author:
        obj.title = text
        obj.save()
        return HttpResponse('ok')
    else:
        return HttpResponse('failfish')


def editTask(request):
    id = request.GET.get('id')
    user = request.GET.get('user')
    text = request.GET.get('text')
    deadline = request.GET.get('deadline')
    if len(deadline)<1:
        deadline = None
    author = User.objects.get(id=user)
    obj = Task.objects.get(id=id)
    if obj.author == author:
        obj.title = text
        obj.deadline = deadline
        obj.save()
        return HttpResponse('ok')
    else:
        return HttpResponse('failfish')
