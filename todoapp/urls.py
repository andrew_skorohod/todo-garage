from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$',views.main,name="main"),
    url(r'^details/(?P<id>\d+)/$', views.projectDetail, name="projectDetail"),
    url(r'^ajax/addproject/$', views.addProject),
    url(r'^ajax/addtask/$', views.addTask),
    url(r'^ajax/change/$', views.change),
    url(r'^ajax/changepos/$', views.changepos),
    url(r'^ajax/deltask/$', views.deleteTask),
    url(r'^ajax/delproject/$', views.deleteProject),
    url(r'^ajax/editproject/$', views.editProject),
    url(r'^ajax/edittask/$', views.editTask),

]