# README #
ссылка на HEROKU https://swtodo.herokuapp.com/
ссылка на задание.
https://drive.google.com/file/d/0Bx5f9K6GrjugZGM1RlZTVTU5QXJsYjVJX25CWXdHbGRrQUUw/view?usp=sharing


### Описание ###
Одностраничное приложение(почти) для работы с проектами/заданиями. Все действия происходят без обновления страницы через Ajax.
Задания имеют приоритет.Приоритетом является его позиция в списке. Чем выше приоритет тем выше позиция.Изменение позиции происходит посредством перетаскивания.Позиции сохраняются при обновлении страницы.
Бекенд написан на python/django.

### How do I get set up? ###


* git clone "your link from the clone"
* virtualenv -p python3 todo garage
* cd todo garage
* source bin/activate
Дальше сложнее. Проект сделан под хероку. В requirements указаны те файлы которые нужны хероку.
Для запуска на локальном сервере нужно кое что изменить.
* pip install django,django-allauth
Добавить в папку todo файл local_settings.py.В файле написать:

import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
DEBUG = True

Дальше провести миграции.
* python manage.py makemigrations
* python manage.py migrate
* python manage.py runserver

localhost:8000


### sql part###

1. SELECT DISTINCT status FROM TASKS ORDER BY status; 
2. SELECT COUNT(*) as quantity,projects.name from TASKS INNER JOIN projects on project_id=projects.id GROUP BY project_id ORDER BY quantity DESC; 
3. SELECT COUNT(*) AS quantity,projects.name 
FROM tasks INNER JOIN projects ON project_id = projects.id 
GROUP BY projects.name 
ORDER BY projects.name ASC; 
4. SELECT * FROM tasks WHERE name LIKE "%N"; 
5. SELECT projects.name,count(tasks.id) as count FROM projects RIGHT JOIN tasks 
on projects.id=tasks.project_id WHERE SUBSTR(projects.name,LENGTH(projects.name)/2+1, 1)= 'a'; 
6. SELECT name,COUNT(*) FROM tasks GROUP BY name HAVING 
COUNT(tasks.name) > 1 order by name; 
7. SELECT COUNT(*), tasks.name , tasks.status FROM tasks GROUP BY tasks.name HAVING COUNT(*) > 1;
8. select projects.name,count(tasks.status) from 
projects INNER JOIN tasks on projects.id=tasks.project_id where 
status='completed' group by name HAVING COUNT(tasks.status) >10 order by name;