//no whitespace in the beginning form validation
function validate(title) {
    if (title.length > 0){
        var re = /^\S.*$/;
        var valid = re.test(title);
        return valid
        }
    else {
        return false
    }
    }

//sortable
$(function() {
    $("#sortable").sortable({
        update: function(event, ui) {
            var id = ui.item.attr("id");
            var nextId = $("#" + id).next().attr("id"); //undefined if no element
            var pId = $("#" + id).prev().attr("id");
            var data = {
                'id': id,
                'nextId': nextId,
                'pId': pId,
            };
            $.ajax({
                data: data,
                url: '/ajax/changepos/',
                type: 'GET',
                success: function(response) {
                    console.log(response);
                }
            });

        }
    });

    $("#sortable").disableSelection();
});

// sortable ends

$(function(){// disabling email sending
    $("#id_email").parent().hide();
});



$(document).on('change', ':checkbox', function() {
    var id = this.id;
    if ($("#" + id).prop('checked') == true) {
        var data = {
            'id': id,
            'status': 'true'
        };
        $.ajax({
            data: data,
            url: '/ajax/change/',
            type: 'GET',
            success: function(response) {
            }
        });
    } else {
        var data = {
            'id': id,
            'status': 'false'
        };
        $.ajax({
            data: data,
            url: '/ajax/change/',
            type: 'GET',
            success: function(response) {
                console.log(response);
            }
        });
    }

});


function addTask() {
    var title = $('#taskTitle').val();
    if (validate(title) === true) {
        var deadline = $('#taskDeadline').val();
        var nextId = $("tbody tr").first().attr("id");
        console.log(nextId);
        var data = {
            'user': user,
            'title': title,
            'deadline': deadline,
            'project': project,
            'nextId': nextId,
        };
        $.ajax({
            data: data,
            url: '/ajax/addtask/',
            type: 'GET',
            success: function(response) {
                $("tbody").prepend('<tr class="row" id="task' + response + '"><td class="col-md-1 text-center"><input type="checkbox" id="' + response + '">\
        </td><td class="col-md-5 text-center">' + title + '</td><td class="col-md-4 text-center">' + deadline + '</td><td class="col-md-2 text-center" \
        id="buttons" ><button class="btn btn-warning "\
         onclick="editTask(' + response + ')">\
        <i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i></button>\
        <button  class="btn btn-danger" onclick="delTask(' + response + ')"><i class="fa fa-times fa-lg" aria-hidden="true"></i></button></td></tr>');
                $('#taskTitle').val('');
                $('#taskDeadline').val('');
            }

        });
    }
}

function delTask(task) {
    var data = {
        'user': user,
        'task': task,
    };
    $.ajax({
        data: data,
        url: '/ajax/deltask/',
        type: 'GET',
        success: function(response) {
            if (response == 'ok') {
                $('#task' + task).remove();
            } else {
                console.log(response);
            }

        }
    });
}

function editTask(task) {
    var old_text = $('#task' + task + ' .col-md-5').text();
    var old_deadline = $('#task' + task + ' .col-md-4').text();
    $('#task' + task + ' .col-md-5').text('');
    $('#task' + task + ' .col-md-5').append('<input type="text" class="form-control" name="task"\
        id="taskTitle' + task + '" \
        value="' + old_text + '">');
    $('#task' + task + ' .col-md-4').text('');
    $('#task' + task + ' .col-md-4').append('<input type="date" class="form-control" name="deadline"\
        id="taskDeadline' + task + '"\
        value="' + old_deadline + '">');
    $('#task' + task + ' .col-md-2 .btn-warning').remove();
    $('#task' + task + ' .col-md-2 .btn-danger').remove();
    $('#task' + task + ' .col-md-2 ').prepend('<button  class="btn btn-success" \
         onclick="saveTask(' + task + ')"><i class="fa fa-check fa-lg" aria-hidden="true"></i></button>');
}


function saveTask(task) {
    var text = $('#taskTitle' + task).val();
    if (validate(text) === true) {
    var deadline = $('#taskDeadline' + task).val();
    var data = {
        'user': user,
        'text': text,
        'id': task,
        'deadline': deadline,
    };
    $.ajax({
        data: data,
        url: '/ajax/edittask/',
        type: 'GET',
        success: function(response) {
            if (response == 'ok') {
                $('#task' + task + ' .col-md-5 input').remove();
                $('#task' + task + ' .col-md-4 input').remove();
                $('#task' + task + ' .col-md-2 .btn-success').remove();
                $('#task' + task + ' .col-md-5 ').append(text);
                $('#task' + task + ' .col-md-4').append(deadline);
                $('#task' + task + ' .col-md-2   ').prepend('<button  class="btn btn-warning"\
                     onclick="editTask(' + task + ')"><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>\
                     </button>');
                $('#task' + task + ' .col-md-2   ').append('<button  class="btn btn-danger" \
                onclick="delTask(' + task + ')"><i class="fa fa-times fa-lg" aria-hidden="true"></i></button>');


            }
        }
    });
    }
}

function addProject() {
    var title = $('#projectForm').val();
    if (validate(title) === true) {
        var data = {
            'user': user,
            'title': title
        };
        $.ajax({
            data: data,
            url: '/ajax/addproject/',
            type: 'GET',
            success: function(response) {
                $("tbody").prepend('<tr id="project' + response + '"><td class="col-md-2">\
         ' + response + '<td class="col-md-8">\
        <a href="/details/' + response + '/">' + title + '</a></td><td class="col-md-2" id="buttons">\
        <button  class="btn btn-warning" onclick="editProject(' + response + ')">\
        <i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i></button>\
        <button  class="btn btn-danger" onclick="delProject(' + response + ')">\
        <i class="fa fa-times fa-lg" aria-hidden="true"></i></button></td></tr>');
                $('#projectForm').val('');
            }
        });
    }
};

function delProject(project) {
    var data = {
        'user': user,
        'project': project,
    };
    $.ajax({
        data: data,
        url: '/ajax/delproject/',
        type: 'GET',
        success: function(response) {
            if (response == 'ok') {
                $('#project' + project).remove();
            } else {
                console.log(response);
            }

        }
    });

}

function editProject(project) {
    var old_text = $('#project' + project + ' .col-md-8 a').text();
    $('#project' + project + ' .col-md-8 a').remove();
    $('#project' + project + ' .col-md-8').append('<input type="text"value="' + old_text + '" id="inp' + project + '">');
    $('#project' + project + ' .col-md-2 .btn-warning').remove();//remove old buttons
    $('#project' + project + ' .col-md-2 .btn-danger').remove();
    $('#project' + project + ' #buttons').prepend('<button  class="btn btn-success" \
         onclick="saveProject(' + project + ')"><i class="fa fa-check fa-lg" aria-hidden="true"></i></button>');
}




function saveProject(project) {
    var text = $('#inp' + project).val();
    if (validate(text) === true) {
    var data = {
        'user': user,
        'id': project,
        'text': text
    };
    $.ajax({
        data: data,
        url: '/ajax/editproject/',
        type: 'GET',
        success: function(response) {
            if (response == 'ok') {
                $('#project' + project + ' .col-md-8 input').remove();
                $('#project' + project + ' .col-md-8 ').append('<a href="/details/' + project + '/" >' + text + '</a>');
                $('#project' + project + ' #buttons .btn-success').remove();//remove confirm button,add old buttons
                $('#project' + project + ' #buttons').prepend('<button  class="btn btn-warning"\
                     onclick="editProject(' + project + ')"><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>\
                     </button>');
                $('#project' + project + ' #buttons').append('<button  class="btn btn-danger" \
                onclick="delProject(' + project + ')">\
                <i class="fa fa-times fa-lg" aria-hidden="true"></i></button>');
            }
        }
    });
    }
}

